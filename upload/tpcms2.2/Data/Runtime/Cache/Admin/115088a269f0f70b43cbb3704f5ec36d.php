<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>系统后台 - Tpcms内容管理系统 - by Tpcms</title>
<?php if(CONTROLLER_NAME == "Login"): ?><link rel="stylesheet" type="text/css" href="/test/tpcms/Data/Public/admin/css/admin_login.css"  />
<link rel="stylesheet" type="text/css" href="/test/tpcms/Data/Public/admin/css/admin_default_color.css" />
<?php else: ?>
<link href="/test/tpcms/Data/Public/admin/css/admin_style.css" rel="stylesheet" />
<link href="/test/tpcms/Data/Public/org/artDialog/skins/default.css" rel="stylesheet" /><?php endif; ?>

<script type='text/javascript'>
MODULE='/test/tpcms/index.php/Admin'; //当前模块
CONTROLLER='/test/tpcms/index.php/Admin/Article'; //当前控制器)
ACTION='/test/tpcms/index.php/Admin/Article/index';//当前方法(方法)
ROOT='/test/tpcms'; //当前项目根路径
PUBLIC= '/test/tpcms/Data/Public/admin';//当前定义的Public目录
</script>
<script src="/test/tpcms/Data/Public/org/wind.js"></script>
<script src="/test/tpcms/Data/Public/org/jquery.js"></script>
</head>
<body class="J_scroll_fixed">
<div class="wrap J_check_wrap">
  <div class="nav">
    <ul class="cc">
    <li <?php if($_GET['verifystate'] == 2): ?>class="current"<?php endif; ?>><a href="<?php echo U('index',array('category_cid'=>$_GET['category_cid'],'verifystate'=>2));?>"><?php echo ($cat["cname"]); ?>列表</a></li>
      <li <?php if($_GET['verifystate'] == 1): ?>class="current"<?php endif; ?>><a href="<?php echo U('index',array('category_cid'=>$_GET['category_cid'],'verifystate'=>1));?>">待审核文章</a></li>
    </ul>
  </div>
  <div class="mb10">
		<a href="<?php echo U('Article/add',array('category_cid'=>I('get.category_cid')));?>"  class="btn" title="添加内容"><span class="add"></span>添加内容</a>
       
   		 <a href="<?php echo U('/'.strtolower($cat['remark'])."_l_".$cat['cid']);?>" target="_blank"  class="btn" title="访问该栏目">访问该栏目</a>
  </div>
  <div class="h_a">搜索</div>
  <form method="get" action="<?php echo U('index');?>">
 
    <div class="search_type cc mb10">
      <div class="mb10"> 
        <span class="mr20">时间：
        <input type="text" name="start_time" class="input length_2 J_date" value="<?php echo ($_GET['start_time']); ?>" style="width:80px;" placeholder="选择时间">-<input type="text" class="input length_2 J_date" name="end_time" value="<?php echo ($_GET['end_time']); ?>" style="width:80px;" placeholder="选择时间">
        <select class="select_2" name="flag"style="width:70px;">
          <option value='0' >全部</option>
          <?php if(is_array(C("flag"))): foreach(C("flag") as $key=>$v): ?><option value="<?php echo ($v); ?>" <?php if(isset($_GET["flag"]) && $_GET["flag"] == $v): ?>selected='selected'<?php endif; ?>><?php echo ($v); ?></option><?php endforeach; endif; ?>
       
        
        </select>
        <select class="select_2" name="keytype" style="width:70px;">
          <option value='article_title'  <?php if(isset($_GET["keytype"]) && $_GET["keytype"] == "article_title"): ?>selected='selected'<?php endif; ?> >标题</option>
          <option value='keywords' <?php if(isset($_GET["keytype"]) && $_GET["keytype"] == "keywords"): ?>selected='selected'<?php endif; ?> >关键词</option>
          <option value='descirption' <?php if(isset($_GET["keytype"]) && $_GET["keytype"] == "descirption"): ?>selected='selected'<?php endif; ?>>描述</option>
          <option value='username'  <?php if(isset($_GET["keytype"]) && $_GET["keytype"] == "username"): ?>selected='selected'<?php endif; ?>>用户名</option>
          <option value='aid'  <?php if(isset($_GET["keytype"]) && $_GET["keytype"] == "aid"): ?>selected='selected'<?php endif; ?>>ID</option>
        </select>
        关键字：
        <input type="text" class="input length_2" name="keyword" style="width:200px;" value="<?php echo ($_GET['keyword']); ?>" placeholder="请输入关键字...">
          <input type="hidden" value="<?php echo ($_GET['category_cid']); ?>" name="category_cid">
          <input type="hidden" value="<?php echo ($_GET['verifystate']); ?>" name="verifystate">
          <button class="btn" type="submit">搜索</button>
        </span>
      </div>
    </div>
  </form>
  <form class="J_ajaxForm" action="" method="post">
    <div class="table_list">
      <table width="100%">
	        <colgroup>
	        <col width="16">
	        <col width="50">
	        <col width="60">
	        <col width="">
	        <col width="80">
	        <col width="90">
	        <col width="140">
	        <col width="120">
	        </colgroup>
	        <thead>
	        <tr>
	            <td><label><input type="checkbox" class="J_check_all" data-direction="x" data-checklist="J_check_x"></label></td>
	            <td>排序</td>
	            <td align="left">ID</td>
	            <td>标题</td>
	            <td align="center">点击量</td>
	            <td align="center">发布人</td>
	            <td align="center"><span>发帖时间</span></td>
	            <td align="center">管理操作</td>
	          </tr>
	        </thead>
          <?php if($data): if(is_array($data)): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$value): $mod = ($i % 2 );++$i;?><tr>
	            <td><input type="checkbox" class="J_check" data-yid="J_check_y" data-xid="J_check_x" name="ids[<?php echo ($value["aid"]); ?>]" value="<?php echo ($value["aid"]); ?>"></td>
	            <td><input name='sort[<?php echo ($value["aid"]); ?>]' class="input mr5"  type='text' size='3' value='<?php echo ($value["sort"]); ?>'></td>
	            <td align="left"><?php echo ($value["aid"]); ?></td>
	            <td>
	            	<a href="<?php echo U('/'.strtolower($value['remark'])."_v_".$value['category_cid'].'_'.$value['aid']);?>" target="_blank">
	            		<?php echo ($value["article_title"]); ?>

                  <?php if($value["flag"]): ?>[<span style="color:red" ><?php echo ($value["flag"]); ?></span>]<?php endif; ?>
	            	</a>
	            </td>
	            <td align="center"><?php echo ($value["click"]); ?></td>
	            <td align="center"><?php echo ($value["username"]); ?></td>
	            <td align="center"><?php echo (format_date($value["addtime"],0)); ?></td>
	            <td align="center">
	             	<a href="<?php echo U('/'.strtolower($value['remark'])."_v_".$value['category_cid'].'_'.$value['aid']);?>"  target="_blank">访问</a>
	             	| 
	            	<a href="<?php echo U('Article/edit',array('category_cid'=>$value["category_cid"],'aid'=>$value["aid"]));?>" >修改</a> 
	            	| 
	            	<a href="<?php echo U('Article/del',array('category_cid'=>$value["category_cid"],'aid'=>$value["aid"],'verifystate'=>$value['verifystate']));?>" class="J_ajax_del" >删除</a>
	            </td>
	        </tr><?php endforeach; endif; else: echo "" ;endif; ?>
        <?php else: ?>
        <tr><td colspan="8">没有找到符合条件的记录</td></tr><?php endif; ?>
	        
        </table>
      	<div class="p10"><div class="pages"> <?php echo ($page); ?> </div> </div>
     
    </div>
    <div class="btn_wrap">
      <div class="btn_wrap_pd">
        <label class="mr20"><input type="checkbox" class="J_check_all" data-direction="y" data-checklist="J_check_y">全选</label>                
        <button class="btn J_ajax_submit_btn" type="submit" data-action="<?php echo U('sort');?>">排序</button>
        <button class="btn J_ajax_submit_btn" type="submit" data-action="<?php echo U('check');?>">审核</button>
        <button class="btn J_ajax_submit_btn" type="submit" data-action="<?php echo U('cancel_check');?>">取消审核</button>
        <button class="btn J_ajax_submit_btn" type="submit" data-action="<?php echo U('batch_delete');?>">删除</button>
      	<button class="btn J_ajax_submit_btn" type="submit" data-action="<?php echo U('operation');?>">设置</button>
      	<button class="btn J_ajax_submit_btn" type="submit" data-action="<?php echo U('cancel_operation');?>">取消设置</button>
      	<select name="opa">
      		<option value="推荐">推荐</option>
      		<option value="头条">头条</option>
      		<option value="图文">图文</option>
      	</select>
      </div>
    </div>
  </form>
</div>
<script type="text/javascript" src="/test/tpcms/Data/Public/admin/js/mod.common.js"></script>

<script>


function view_comment(obj) {
	Wind.use('artDialog','iframeTools', function () {
         art.dialog.open($(obj).attr("data-url"), {
			close:function(){
				$(obj).focus();
			},
            title: $(obj).attr("data-title"),
			width:"800px",
            height: '520px',
			id:"view_comment",
            lock: true,
            background:"#CCCCCC",
            opacity:0
        });
    });
}


</script>
</body>
</html>