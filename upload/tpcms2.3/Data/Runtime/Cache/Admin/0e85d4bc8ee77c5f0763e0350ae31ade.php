<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>系统后台 - Tpcms内容管理系统 - by Tpcms</title>
<?php if(CONTROLLER_NAME == "Login"): ?><link rel="stylesheet" type="text/css" href="/dev/Data/Public/admin/css/admin_login.css"  />
<link rel="stylesheet" type="text/css" href="/dev/Data/Public/admin/css/admin_default_color.css" />
<?php else: ?>
<link href="/dev/Data/Public/admin/css/admin_style.css" rel="stylesheet" />
<link href="/dev/Data/Public/org/artDialog/skins/default.css" rel="stylesheet" /><?php endif; ?>

<script type='text/javascript'>
MODULE='/dev/index.php/Admin'; //当前模块
CONTROLLER='/dev/index.php/Admin/Cache'; //当前控制器)
ACTION='/dev/index.php/Admin/Cache/cache';//当前方法(方法)
ROOT='/dev'; //当前项目根路径
PUBLIC= '/dev/Data/Public/admin';//当前定义的Public目录
</script>
<script src="/dev/Data/Public/org/wind.js"></script>
<script src="/dev/Data/Public/org/jquery.js"></script>
</head>
<body class="J_scroll_fixed">
<div class="wrap J_check_wrap">
	<div class="nav">
		<ul class="cc">
			
			<li class="current">
				<a href="javascript:;">更新缓存</a>
			</li>
		</ul>
	</div>

	
		<form action="<?php echo U('cache');?>" method="post" >
		    <div class="table_full">
			<div class="h_a">更新缓存</div>
			<table width='100%'  class="table_form">
				<tr>
					<th width="200" style='vertical-align:middle'>选择更新</th>
					<td>
						<table>
							<tbody>
								<tr>
									<td>
										<label>
											<input type="checkbox" name="action[]" value="Config" checked='checked'/>
											更新网站配置
										</label>
									</td>
								</tr>

								<tr>
									<td>
										<label>
											<input type="checkbox" name="action[]" value="Category" checked='checked'/>
											栏目缓存
										</label>
									</td>
								</tr>
								<tr>
									<td>
										<label>
											<input type="checkbox" name="action[]" value="Table" checked='checked'/>
											数据表缓存
										</label>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</table>
			
		</div>

			<div class="">
				<div class="btn_wrap_pd">
					<button class="btn btn_submit mr10 " type="submit">更新缓存</button>

				</div>
			</div>
		</form>
		



</div>
<script type="text/javascript" src="/dev/Data/Public/admin/js/mod.common.js"></script>

</body>
</html>