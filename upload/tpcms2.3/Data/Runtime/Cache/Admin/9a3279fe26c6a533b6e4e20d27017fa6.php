<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>系统后台 - Tpcms内容管理系统 - by Tpcms</title>
<?php if(CONTROLLER_NAME == "Login"): ?><link rel="stylesheet" type="text/css" href="/dev/Data/Public/admin/css/admin_login.css"  />
<link rel="stylesheet" type="text/css" href="/dev/Data/Public/admin/css/admin_default_color.css" />
<?php else: ?>
<link href="/dev/Data/Public/admin/css/admin_style.css" rel="stylesheet" />
<link href="/dev/Data/Public/org/artDialog/skins/default.css" rel="stylesheet" /><?php endif; ?>

<script type='text/javascript'>
MODULE='/dev/index.php/Admin'; //当前模块
CONTROLLER='/dev/index.php/Admin/User'; //当前控制器)
ACTION='/dev/index.php/Admin/User/edit';//当前方法(方法)
ROOT='/dev'; //当前项目根路径
PUBLIC= '/dev/Data/Public/admin';//当前定义的Public目录
</script>
<script src="/dev/Data/Public/org/wind.js"></script>
<script src="/dev/Data/Public/org/jquery.js"></script>
</head>
<body class="J_scroll_fixed">
<div class="wrap J_check_wrap">
	<div class="nav">
    <ul class="cc">
       	<li><a href="<?php echo U('User/index',array('role'=>$_GET['role']));?>"><?php if($_GET["role"] == 2): ?>会员<?php else: ?>管理员<?php endif; ?>列表</a></li>
        <li class="current"><a href="javascript:;">编辑<?php if($_GET["role"] == 2): ?>会员<?php else: ?>管理员<?php endif; ?></a></li>
      </ul>
	</div>
    <div class="h_a"><?php if($_GET["role"] == 2): ?>会员<?php else: ?>管理员<?php endif; ?>属性</div>
  	<form action="<?php echo U('User/edit');?>" method="post" class="J_ajaxForms"  name="myform" id="myform" >
    <div class="table_full">
      <table width="100%"  class="table_form">
        <?php if($_GET["role"] == 2): ?><tr>
          <th width="120">会员等级：</th>
          <td class="y-bg">

          	<select name="grade_gid" id="grade_gid">
          		<option value="">请选择会员等级</option>
          		<?php if(is_array($grade)): $i = 0; $__LIST__ = $grade;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><option value = '<?php echo ($v["gid"]); ?>' <?php if($data["grade_gid"] == $v["gid"]): ?>selected='selected'<?php endif; ?>><?php echo ($v["gname"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
          	</select>
          
          </td>
        </tr>
        <?php else: ?>
        <tr>
          <th width="120">角色：</th>
          <td class="y-bg">
              <?php if($data["uid"] == 1): ?>超级管理员
            <?php else: ?>
              <table>
                  <?php if(is_array($data["access"])): foreach($data["access"] as $key=>$a): ?><tr>
                      <?php if($key == 0): ?><th class='hand' onclick='add_group(this)'>[+]</th>
                      <?php else: ?>
                      <th class='hand' onclick='del_group(this)'>[-]</th><?php endif; ?>
                  <td>
                      <select name="group_id[]">
                      <?php if(is_array($authGroup)): $i = 0; $__LIST__ = $authGroup;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><option value="<?php echo ($v["id"]); ?>" <?php if($v["id"] == $a): ?>selected='selected'<?php endif; ?>><?php echo ($v["title"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                      </select>
                  </td>
                  </tr><?php endforeach; endif; ?>
              </table><?php endif; ?>
          </td>
        </tr><?php endif; ?>
        <tr>
          <th width="120">用户名称：</th>
          <td class="y-bg"><span class="must_red">*</span><input type="text" class="input" name="username" id="username" size="30" value="<?php echo ($data["username"]); ?>" /></td>
        </tr>
        <tr>
          <th width="120">用户昵称：</th>
          <td class="y-bg"><span class="must_red">*</span><input type="text" class="input" name="nickname" id="nickname" size="30" value="<?php echo ($data["nickname"]); ?>" /></td>
        </tr>
         <tr>
          <th width="120">邮箱：</th>
          <td class="y-bg"><span class="must_red">*</span><input type="text" class="input" name="email" id="email" size="30" value="<?php echo ($data["email"]); ?>" /></td>
        </tr>
        <tr>
          <th width="120">用户密码：</th>
          <td class="y-bg"><input type="password" class="input" name="password" id="password" size="30" value="" /> 不填写，则不修改</td>
        </tr>
        <tr>
          <th width="120">用户确认密码：</th>
          <td class="y-bg"><input type="password" class="input" name="passwords" id="passwords" size="30" value="" /> 不填写，则不修改</td>
        </tr>
        <tr>
          <th>状态：</th>
          <td class="y-bg">

          	<label>
          		<input type="radio" name="is_lock" value="1" <?php if($data["is_lock"] == 1): ?>checked='checked'<?php endif; ?>> 锁定
          	</label>

          	<label>
          		<input type="radio" name="is_lock" value="0" <?php if($data["is_lock"] == 0): ?>checked='checked'<?php endif; ?>> 正常
          	</label>
          </td>
        </tr>
       
      </table>
    </div>
    <div class="">
      <div class="btn_wrap_pd">
      	<input type="hidden" name="uid" value="<?php echo ($data["uid"]); ?>">
      	<input type="hidden" name='role' value="<?php echo ($_GET['role']); ?>" />
        <button class="btn btn_submit mr10 J_ajax_submit_btn" type="submit">编辑</button>
     
      </div>
    </div>
  </form>
</div>
<script>
  var userValidate={ 
        rules: {
          "grade_gid":{
          required:1
        },
        "username":{
          required:1
        },
        "nickname":{
          required:1
        },
        "email":{
          required:1,
          email:true,
        },
        "password":{
          
        },
        "passwords":{
          
          equalTo:"#password"
          
        },
      },
           
      messages: {
          "grade_gid":{
          required:"请选择会员等级"
        },
        "username":{
          required:"会员名称不能为空！"
        },
        "nickname":{
          required:"会员昵称称不能为空！"
        },
        "email":{
          required:"邮箱不能为空！",
          email:"邮箱格式不对！"
        },
        "password":{
          
        },
        "passwords":{
          equalTo:"两次密码不一致！"
        },
        
      }};
</script>
<script type="text/javascript" src="/dev/Data/Public/admin/js/mod.common.js"></script>
<script type="text/javascript" src="/dev/Data/Public/admin/js/mod.content_addtop.js"></script>
<script type="text/javascript" src="/dev/Data/Public/admin/js/mod.user.js"></script>
</body>
</html>