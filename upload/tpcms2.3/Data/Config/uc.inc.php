<?php
$ucenter = include_once 'ucenter.inc.php';
//ucserver数据库配置信息
$dbhost = $ucenter['dbhost'];			// 数据库服务器
$dbuser = $ucenter['dbuser'];			// 数据库用户名
$dbpw =$ucenter['dbpw'];				// 数据库密码
$dbname =$ucenter['dbname'];			// 数据库名
$pconnect = 0;				            // 数据库持久连接 0=关闭, 1=打开
$tablepre = $ucenter['tablepre'];   	// 表名前缀, 同一数据库安装多个论坛请修改此处
$dbcharset = 'utf8';			        // MySQL 字符集, 可选 'gbk', 'big5', 'utf8', 'latin1', 留空为按照论坛字符集设定

//同步登录 Cookie 设置
$cookiedomain = ''; 			// cookie 作用域
$cookiepath = '/';			    // cookie 作用路径


//通信相关
define('UC_KEY', $ucenter['uc_key']);				      // 与 UCenter 的通信密钥, 要与 UCenter 保持一致
define('UC_API', $ucenter['uc_api']);	  // UCenter 的 URL 地址, 在调用头像时依赖此常量
define('UC_CHARSET', 'utf8');				          // UCenter 的字符集
define('UC_IP', '');					              // UCenter 的 IP, 当 UC_CONNECT 为非 mysql 方式时, 并且当前应用服务器解析域名有问题时, 请设置此值
define('UC_APPID', 1);