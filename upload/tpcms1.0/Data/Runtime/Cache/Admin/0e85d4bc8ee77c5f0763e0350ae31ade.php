<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>更新全站缓存</title>
	<script type='text/javascript' src='/dcms/Core/Org/Jquery/jquery-1.8.2.min.js'></script>
	<link href='/dcms/Core/Org/hdjs/hdjs.css' rel='stylesheet' media='screen'>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/hdjs.min.js'></script>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/org/cal/lhgcalendar.min.js'></script>
	<script type='text/javascript'>
		MODULE='/dcms/index.php/Admin'; //当前模块
		CONTROLLER='/dcms/index.php/Admin/Cache'; //当前控制器)
		ACTION='/dcms/index.php/Admin/Cache/cache';//当前方法(方法)
		ROOT='/dcms'; //当前项目根路径
		PUBLIC= '/dcms/Core/Tpcms/Admin/View/Public';//当前定义的Public目录
	</script>

	<link rel="stylesheet" type="text/css" href="/dcms/Core/Tpcms/Admin/View/Public/css/mod.base.css" />
</head>
<body>

	<form method="post" action="">
		<div class="hd-title-header">温馨提示</div>
		<div class="help">
			<ul>
				<li>首次安装必须更新全站缓存</li>
			</ul>
		</div>
		<div class="hd-title-header">更新缓存</div>

		<table class="hd-table hd-table-form hd-form">
			<tr>
				<th class="hd-w100">选择更新</th>
				<td>
					<table class="table2" width="100%">
						
						<tr>
							<td>
								<label>
									<input type="checkbox" name="action[]" value="Config" checked=''/>
									更新网站配置
								</label>
							</td>
						</tr>

						<tr>
							<td>
								<label>
									<input type="checkbox" name="action[]" value="Category" checked=''/>
									栏目缓存
								</label>
							</td>
						</tr>
						<tr>
							<td>
								<label>
									<input type="checkbox" name="action[]" value="Table" checked=''/>
									数据表缓存
								</label>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>

		<input type="submit" value="开始更新" class="hd-btn hd-btn-xm"/>

	</form>

</body>
</html>