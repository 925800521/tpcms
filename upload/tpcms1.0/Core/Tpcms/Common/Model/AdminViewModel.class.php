<?php
/** [会员视图模型]
 * @Author: 976123967@qq.com
 * @Date:   2015-04-16 16:40:07
 * @Last Modified by:   happy
 * @Last Modified time: 2015-05-01 19:48:41
 */
namespace Common\Model;
use Think\Model\ViewModel;
class AdminViewModel extends ViewModel{
	public $tableName ='user';
	public $viewFields  = array(
		'user'=>array(
			'*',
			'_type'=>'INNER',
		),
	); 
}