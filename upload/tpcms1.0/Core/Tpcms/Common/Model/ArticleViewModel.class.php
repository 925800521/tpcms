<?php
/** [文档试图模型]
 * @Author: 976123967@qq.com
 * @Date:   2015-04-14 18:41:12
 * @Last Modified by:   happy
 * @Last Modified time: 2015-05-01 19:47:17
 */
namespace Common\Model;
use Think\Model\ViewModel;
class ArticleViewModel extends ViewModel{

	public $tableName = 'article';

	public $viewFields  = array(
		'article'=>array(
			'*',
			'_type'=>'INNER',
		),
		'user'=>array(
			'username','uid',
			'_type'=>'INNER',
			'_on' =>'user.uid=article.user_uid',
		),
		'category'=>array(
			'cid','cname','remark',
			'_type'=>'INNER',
			'_on' =>'category.cid=article.category_cid',
		)
	); 
}