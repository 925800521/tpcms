<?php
/**[***]
 * @Author: 976123967@qq.com
 * @Date:   2015-04-29 12:25:03
 * @Last Modified by:   happy
 * @Last Modified time: 2015-05-01 19:43:07
 */
namespace Common\Model;
use Think\Model;
class RArticleModel extends Model{


	public function alter_article($aid)
	{
		$raid = I('post.raid');
		$this->where(array('article_aid'=>$aid))->delete();
		if($raid)
		{
			foreach($raid as $v)
			{
				if($v)
				{
					$data[] = array(
						'raid'=>$v,
						'article_aid'=>$aid,
					);	
				}
			}
			$this->addAll($data);
		}
	}
}