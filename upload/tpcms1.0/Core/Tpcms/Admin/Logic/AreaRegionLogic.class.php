<?php
/** [配送地区和区域表逻辑模型]
 * @Author: 976123967@qq.com
 * @Date:   2015-02-06 12:41:19
 * @Last Modified by:   Administrator
 * @Last Modified time: 2015-05-04 10:33:20
 */
namespace Admin\Logic;
use Think\Model;
class AreaRegionLogic extends Model{
	protected $tableName = 'relation_shipping_area_region';

	public $model;
	public function _initialize()
	{
		$this->model = D('AreaRegion');
	}

	/**
	 * [add_area_region 添加数据]
	 * @param [type] $areaId [description]
	 */
	public function add_area_region($areaId)
	{
		// 删除
		$this->model->delete_area_region_by_shipping_area_area_id($areaId);

		$top    = I('post.top');
		$first  = I('post.first');
		$second = I('post.second');
		$third  = I('post.third');

		$data = array();
		foreach($top as $k=> $v)
		{
			if($third[$k])
				$data['region_region_id'] = $third[$k];
			elseif($second[$k])
				$data['region_region_id'] = $second[$k];
			elseif($first[$k])
				$data['region_region_id'] = $first[$k];
			elseif($top[$k])
				$data['region_region_id'] = $top[$k];

			$data['shipping_area_area_id'] = $areaId;
			$this->add($data);
		}
	}

	/**
	 * [find_all_by_shipping_area_area_id 读取数据通过关联外键shipping_area_area_id]
	 * @param  [type] $areaId [description]
	 * @return [type]         [description]
	 */
	public function find_all_by_shipping_area_area_id($areaId)
	{
		$data = $this->model->find_all_by_shipping_area_area_id($areaId);

		if(!$data) return false;

		$regionLogic = D('Region','Service');
		foreach($data as $k => $v)
		{
			$data[$k]['area'] = $regionLogic->get_array_region($v['region_region_id']);

		}

		return $data;
	}

}