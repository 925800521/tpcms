<?php
/**[后台默认访问控制器]
 * @Author: happy
 * @Email:  976123967@qq.com
 * @Date:   2015-03-14 15:00:55
 * @Last Modified by:   Administrator
 * @Last Modified time: 2015-07-09 11:23:12
 */
namespace Admin\Controller;
class IndexController extends PublicController
{
	/**
	 * [index 欢迎信息]
	 * @return [type] [description]
	 */
	public function index()
	{
	
		$topMenu = array(
            1=>array('menu'=>'内容'),
            4=>array('menu'=>'会员'),
            2=>array('menu'=>'系统'),
            3=>array('menu'=>'我的面板'),
            5=>array('menu'=>'模板'),
        );
        $this->assign('topMenu',$topMenu);
        $menu= array(
            1=>array(
                array(
                    'menu'=>'内容管理',
                    'child'=>array(
                                array('id'=>11,'menu'=>'管理内容','url'=>U('Article/welcome')),
                              	array('id'=>12,'menu'=>'文档类型','url'=>U('Type/index')),
                                array('id'=>13,'menu'=>'栏目列表','url'=>U('Category/index')),
                                array('id'=>16,'menu'=>'留言列表','url'=>U('Feedback/index')),   
                           
                     )
                ),
                array(
                    'menu'=>'广告管理',
                    'child'=>array(
                                array('id'=>14,'menu'=>'位置管理','url'=>U('Position/index')),
                                array('id'=>15,'menu'=>'广告列表','url'=>U('Ad/index',array('verifystate'=>2))),         
                     )
                ),
               
                /*array(
                    'menu'=>'静态生成',
                    'child'=>array(
                                array('id'=>17,'menu'=>'栏目生成','url'=>U('Build/category')),
                                array('id'=>18,'menu'=>'内容生成','url'=>U('Build/article')),
                        )
                ),*/
                array(
                    'menu'=>'内容相关管理',
                    'child'=>array(
                                array('id'=>20,'menu'=>'附件管理','url'=>U('Upload/index')),
                                array('id'=>21,'menu'=>'模型管理','url'=>U('Model/index')),
                                array('id'=>22,'menu'=>'属性管理','url'=>U('Flag/index'))
                        )
                ),
            ),
            2=>array(
                array(
                    'menu'=>'系统设置',
                    'child'=>array(
                              array('id'=>25,'menu'=>'网站配置','url'=>U('Config/index')),
                              array('id'=>105,'menu'=>'数据库备份','url'=>U('Backup/index')),
                      )
                    ),
                array(
                    'menu'=>'管理员设置',
                    'child'=>array(
                              array('id'=>26,'menu'=>'管理员管理','url'=>U('User/index',array('role'=>1))),
                              array('id'=>27,'menu'=>'用户组管理','url'=>U('AuthGroup/index')),
                              array('id'=>28,'menu'=>'规则管理','url'=>U('AuthRule/index'))
                            )
                 ),    
              ), 
            3=>array(
                array(
                    'menu'=>'个人信息',
                    'child'=>array(
                                array('id'=>31,'menu'=>'修改个人信息','url'=>U('User/info')),
                                array('id'=>32,'menu'=>'修改密码','url'=>U('User/change'))
                        )
                    ),  
               ), 
           4=>array(
                array(
                    'menu'=>'会员管理',
                    'child'=>array(
                            array('id'=>41,'menu'=>'会员列表','url'=>U('User/index',array('role'=>2))),
                           	array('id'=>42,'menu'=>'会员等级','url'=>U('UserGrade/index')),
                        ), 
                    ), 
                array(
                    'menu'=>'评论管理',
                    'child'=>array(
                            array('id'=>49,'menu'=>'所有评论','url'=>U('UserComment/index')),      
                        )  
                     ),  
              ),
         	5=>array(
         		array(
	                'menu'=>'模板管理',
	                'child'=>array(
	                        array('id'=>51,'menu'=>'模板风格','url'=>U('Templates/index')),
	                       	
	                    ), 
	                ), 

         	),
        );
	 	$this->assign('menu',$menu);	
		$this->display();
	}

	/**
	 * [get_mysql_version 数据库版本]
	 * @return [type] [description]
	 */
	public function get_mysql_version()
	{
		$user = M();
		$mysqlinfo=mysql_get_server_info();
		return $mysqlinfo;
	}
	
	/**
	 * [get_os 获取操作系统]
	 * @return [type] [description]
	 */
	public function get_os()
	{
		$agent = $_SERVER['HTTP_USER_AGENT']; //获取操作系统、浏览器等信息
		$os = ''; //返回操作系统
		
		if(eregi('win', $agent) && eregi('nt 6.3', $agent))
		{
			$os = 'Windows 8.1';
		}
		elseif(eregi('win', $agent) && eregi('nt 6.2', $agent))
		{
			$os = 'Windows 8';
		}
		else if(eregi('win', $agent) && eregi('nt 6.1', $agent))
		{
			$os = 'Windows 7';
		}
		else if(eregi('win', $agent) && eregi('nt 6.0', $agent))
		{
			$os = 'Windows Vista';
		}
		else if(eregi('win', $agent) && eregi('nt 5.2', $agent))
		{
			$os = "Windows 2003"; //eregi字符串比对
		}
		else if(eregi('win', $agent) && eregi('nt 5.1', $agent))
		{
			$os = 'Windows XP';
		}
		else if(eregi('win', $agent) && eregi('nt 5', $agent))
		{
			$os = 'Windows 2000';
		}
		else if(eregi('win', $agent) && eregi('nt', $agent))
		{
			$os = 'Windows NT';
		}
		else if(eregi('linux', $agent))
		{
			$os = 'Linux';
		}
		else if(eregi('unix', $agent))
		{
			$os = 'Unix';
		}
		else if(eregi('sun', $agent) && eregi('os', $agent))
		{
			$os = 'SunOS';
		}
		else if(eregi('ibm', $agent) && eregi('os', $agent))
		{
			$os = 'IBM OS/2';
		}
		else if(eregi('Mac', $agent) && eregi('PC', $agent))
		{
			$os = 'Macintosh';
		}
		else if(eregi('PowerPC', $agent))
		{
			$os = 'PowerPC';
		}
		else if(eregi('AIX', $agent))
		{
			$os = 'AIX';
		}
		else if(eregi('HPUX', $agent))
		{
			$os = 'HPUX';
		}
		else if(eregi('NetBSD', $agent))
		{
			$os = 'NetBSD';
		}
		else if(eregi('BSD', $agent))
		{
			$os = 'BSD';
		}
		else if(ereg('OSF1', $agent))
		{
			$os = 'OSF1';
		}
		else if(ereg('IRIX', $agent))
		{
			$os = 'IRIX';
		}
		else if(eregi('FreeBSD', $agent))
		{
			$os = 'FreeBSD';
		}
		else if(eregi('teleport', $agent))
		{
			$os = 'teleport';
		}
		else if(eregi('flashget', $agent))
		{
			$os = 'flashget';
		}
		else if(eregi('webzip', $agent))
		{
			$os = 'webzip';
		}
		else if(eregi('offline', $agent))
		{
			$os = 'offline';
		}
		else
		{
			$os = 'Unknown';
		}
		return $os;
	}

	/**
	 * [copyright 系统信息]
	 * @return [type] [description]
	 */
	public function copyright()
	{
		$info = array(
			'TPCMS版本'=> C('TPCMS_VERSION'),
			'操作系统' => $this->get_os(),
			'运行环境' => $_SERVER['SERVER_SOFTWARE'],
			'PHP版本' => PHP_VERSION,
			'PHP运行方式' => php_sapi_name(),
			'数据库' => C('DB_TYPE').' '.$this->get_mysql_version(),
			'ThinkPHP版本' => THINK_VERSION,
			'最大上传附件' => ini_get('upload_max_filesize'),
		);
		$this->assign('info',$info);
		
		//判断$info奇偶，奇数需要增加两列
		if(count($info)%2 != 0)
		{
			$this->assign('infonum',1); //1表示奇数，需要补全表格
		}
		$this->display();
	}
}