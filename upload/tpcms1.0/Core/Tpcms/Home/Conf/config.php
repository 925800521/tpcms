<?php

$configRule  = include './Data/Config/rule.inc.php';
$article = include './Data/Config/article.inc.php';
$category = include './Data/Config/category.inc.php';
$articleRule = array();

// 自定义文档文件名
if($article)
{
	foreach ($article as  $v) 
	{

		$articleRule[key($v)]=current($v) ;
	}
}
// 自定义栏目文件文件名
$categoryleRule = array();
if($category)
{
	foreach ($category as  $v) 
	{
		foreach ($v as  $key=>$value) 
		{
			$categoryleRule[$key]=$value ;

		}
	}
}

$rule['URL_ROUTE_RULES']  = array_merge($configRule,$articleRule,$categoryleRule);
$config = array(
	//'配置项'=>'配置值'
	'TMPL_FILE_DEPR'=>'_',
	// 模板替换
	'TMPL_PARSE_STRING'  =>array(
   		'__PUBLIC__'=>__ROOT__.'/Templates',
	),
	'TMPL_DETECT_THEME'=>false,//自动侦测模板主题
	'THEME_LIST'=>'default',//支持的模板主题列表
	// 'DEFAULT_THEME'=>'default',//默认模板主题

	'VIEW_PATH'=>'./Templates/', //更改项目模板的路径

	/******************************路由规则**********************************/
	'URL_ROUTER_ON'=>true,
	
	
);
return array_merge($rule,$config);